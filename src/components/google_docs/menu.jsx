import React,{ Component } from 'react'
import api from '../../services/api'
import { UncontrolledAlert, Spinner} from 'reactstrap';


class Menu extends Component{

	constructor(props){
		super(props)
		this.state ={
			doctype: '',
			processo: {},
			auth_conta: false,
			auth_error: false,
			doc_gerado: false,
			doc_gerado_err: false,
			doc_load: false,
		}
	}

	carregarProcesso = () =>{
		this.setState({processo: this.props.dados_proc})
	}


	handleDoctype = (e) =>{
		this.setState({doctype:e.target.value})
	}

	makeConnection = () =>{
		this.setState({auth_conta: false, auth_error:false})
		api.get('docs/connection/')
		.then(res => this.setState({auth_conta: true}))
		.then(res => this.carregarProcesso())
		.catch(err => this.setState({auth_error:true}))
	}

	criarDocumento = () =>{
		{this.state.doctype &&
		this.setState({doc_gerado:false, doc_gerado_err:false, doc_load:true})	
		api.post('docs/gerarDocumento/',{
			'doc_modelo': this.state.doctype,
			'nome_processo':this.state.processo.nome_processo,
			'codigo_processo':this.state.processo.codigo,
			'objetivo_processo': this.state.processo.objetivo,
			'cadeia_valor':this.state.processo.macroProcesso_primario.nome_macroprocesso,
			'processo_gestor':this.state.processo.gestorPrincipal,
			'processo_proprietario':this.state.processo.proprietario
		})
		.then(res => this.setState({doc_gerado:true, doc_load:false}))
		.catch(err => this.setState({doc_gerado_err:true, doc_load:false}))
		}
	}

	render(){
		return(
			<>
			{this.state.auth_conta &&
				<UncontrolledAlert className="alertas" color="success">Conexão estabelecida com sucesso</UncontrolledAlert>
			}
			{this.state.auth_error &&
				<UncontrolledAlert className="alertas" color="danger">Falha no estabelecimento de conexão</UncontrolledAlert>
			}
			{this.state.doc_gerado &&
				<UncontrolledAlert className="alertas" color="success">Documento gerado com sucesso</UncontrolledAlert>
			}
			{this.state.doc_gerado_err &&
				<UncontrolledAlert className="alertas" color="danger">Ocorreu um erro ao gerar o documento</UncontrolledAlert>
			}

			<div class="row mb-3 justify-content-end">
			{this.state.auth_conta &&
			<div class="form-group col-md-4">
	 	            <select
	              id="inputState"
	              class="form-control"
	              onChange={(e) => this.handleDoctype(e)}
	            >
	              <option selected value="">Selecione o tipo de documento ...</option>
	              <option value="template A" >Documento 1</option>
	              <option value="template B" >Documento 2</option>
	            </select>
	          </div>
	      	}
	          <div class="col-md-3">
	          {this.state.auth_conta ?
	          	<button
	            type="button"
	            class="btn btn-sm btn-success btn-block"
	            onClick={this.criarDocumento}
	          >
	           	Gerar documento   

	          </button>
	          :
	          <button
	            type="button"
	            class="btn btn-sm btn-warning btn-block"
	            onClick={this.makeConnection}
	          >
	           	Estabelecer conexao google docs
	          </button>
	      	}
	        </div>
	        {this.state.doc_load &&
	        <Spinner size="sm" color="primary"/>
	    	}
		</div>
		</>
		)
	}
}

export default Menu