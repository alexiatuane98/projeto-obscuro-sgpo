import React, { useState, useEffect } from "react";
import { Button, Input, FormGroup} from "reactstrap";
import "./pesquisa.css";
import Footer from "../footer/footer";
import api from "../../services/api";
import TableProcessos from './table'
import { UncontrolledAlert } from 'reactstrap';

const Pesquisa = (props) => {

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [splitButtonOpen, setSplitButtonOpen] = useState(false);

  const [macroprocesso, setMacroprocesso] = useState("");
  const [processo, setProcesso] = useState([]);
  const [ord, setOrd] = useState("");
  const [emptyMacro, setEmptyMacro] = useState(false);

  const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

  const toggleSplit = () => setSplitButtonOpen(!splitButtonOpen);


  const searchMacroprocesso = () =>{
    setEmptyMacro(false)
    if(macroprocesso !== "")
      api.get(
            `/processos/?cod_processo=&processo_nome=&macroprocesso=${macroprocesso}
            &proprietario=&gestorPrincipal=&ordering=${ord}`
          )
          .then((res) => {
            const list_processo = res.data.results;
            setProcesso(list_processo);
          })
          .catch((err) => console.log(err))
    else{
      setEmptyMacro(true)
    }
  }

  return (
    <>
      {emptyMacro &&
        <UncontrolledAlert className="alertas" color="warning">Selecione um macroprocesso antes de pesquisar</UncontrolledAlert>
      }
      <div class="row">
        <div class="col-md-4 offset-md-3">
          <div class="form-group ">
            <select
              id="inputState"
              class="form-control"
              onChange={(event) => setMacroprocesso(event.target.value)}
            >
              <option selected>Selecione o macro-processo ...</option>
              {props.data.map((macroprocesso) => (
                <option key={macroprocesso.id} value={macroprocesso.nome_macroprocesso}>
                  {macroprocesso.nome_macroprocesso}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div class="col-md-2">
          <button
            type="button"
            class="btn btn-success btn-block"
            onClick={searchMacroprocesso}
          >
            Pesquisar    
          </button>
        </div>
      </div>
      {
      processo.length > 0 &&
      <div className="row">
        <div class="col-md-4 offset-md-3">
             <div class="form-group">
              <select
              id="inputState"
              class="form-control"
              onChange={(event) => setOrd(event.target.value)}
              >
                <option selected>Selecione a ordenação ...</option>
                <option value="nome_processo">Nome processo ascendente</option>
                <option value="-nome_processo">Nome processo descendente</option>
                <option value="codigo">Codigo processo ascendente</option>
                <option value="-codigo">Código processo descendente</option>
              </select>
             </div>
        </div>
        <div class="col-md-2">
            <button
              type="button"
              class="btn btn-primary btn-block"
              onClick={searchMacroprocesso}
            >
              Ordenar   
            </button>
        </div> 
      </div>
      }
      <TableProcessos processos={processo} />
    </>
  );
};

export default Pesquisa;
