import React,{Component} from 'react'
import {
  Card, CardTitle,  CardDeck, CardBody,ListGroupItem, ListGroup, UncontrolledAlert
} from 'reactstrap';
import api from '../../services/api'
import './main.css'
import Pesquisa from './pesquisa'
import Footer from '../footer/footer'
import TableProcessos from './table'


/*
Criei um componente Main para tratar da principal funcionalidade da cdu pesquisa detalhada.
Aqui será buscado os dados na api e também onde será apresentado a tabela com os processos
e seus respectivos filtros.

Séra o index da página.
*/
class Main extends Component{

	constructor(props){
		super(props)
		this.state = {
			//Aqui ficarão os estados
			componentsFinalistico: [],
			componentsApoio: [],
			componentsDirecionador: [],
			macroprocessos:[],
			isCarregado: false,
			isEmpty: false
		}
	}

	resetState = () =>{
		this.setState({isCarregado:false, isEmpty:false, macroprocessos:[]})
	}

	componentDidMount() {
        this.getComponents();
	}
	
	componentAction = (componente) => {
		this.getMacroprocessos(componente);
	};
	
	getComponents = () => {
        try {
            api.get('componentes').then(res=>{
				const components = res.data;
				
				let filterComponentTypeDirecionador = (item) => {
					return item.tipo === 'direcionador';
				}
				let filterComponentTypeFinalistico = (item) => {
					return item.tipo === 'finalistico';
				}
				let filterComponentTypeApoio = (item) => {
					return item.tipo === 'apoio';
				}
				
				const listComponentsFinalistico = components.filter(filterComponentTypeFinalistico);
				const listComponentsDirecionador = components.filter(filterComponentTypeDirecionador);
				const listComponentsApoio = components.filter(filterComponentTypeApoio);
				
				this.setState({
					componentsFinalistico:listComponentsFinalistico,
					componentsApoio:listComponentsApoio,
					componentsDirecionador: listComponentsDirecionador
				});
			})
        } catch (error) {
			console.log(error);
        }
	}
	
	getMacroprocessos = (componentId) => {
            api.get('macroprocessos').then(res=>{
				const macroprocessos = res.data;
				let filterMacroprocessoByComponent = (item) => {
					return item.componente_primario === componentId;
				}		
				
				const listMacroprocessos = macroprocessos.filter(filterMacroprocessoByComponent);							
				
				//Reseta o estado dos Alerts e do select da pesquisa dos macroprocessos
				this.resetState()

				if(listMacroprocessos.length > 0){
					this.setState({macroprocessos:listMacroprocessos, isCarregado:true})	
				}else{
					this.setState({isEmpty:true})		
				}
			}).catch(err => console.log(err))
	}
	
	render(){
		const {componentsDirecionador, componentsFinalistico, componentsApoio, isCarregado, isEmpty} = this.state
		return(
			<>
			{isCarregado &&
				<UncontrolledAlert className="alertas" color="success">Macroprocessos do componente carregados</UncontrolledAlert>
			}
			{isEmpty &&
				<UncontrolledAlert className="alertas" color="dark">Componente não possui macroprocessos</UncontrolledAlert>
			}
				<CardDeck className='Card'>
					<div>
						<header/>
					</div>
				<Card>			  
				<CardBody className="">
					<CardTitle className="TipoComponente" tag="h5">Finalístico</CardTitle>
					<ListGroup className="Componente card-list">
						{componentsFinalistico.length > 0 ?
							componentsFinalistico.map(component => 
							<ListGroupItem key={component.id} tag="button" onClick={() => this.componentAction(component.id)}>{component.nome_componente}</ListGroupItem>
						)
							:
							<p>Não existem componentes finalísticos</p>
						}
					</ListGroup>
				</CardBody>
				</Card>
				<Card>			  
				<CardBody>
					<CardTitle className="TipoComponente" tag="h5">Direcionador</CardTitle>
					<ListGroup className="Componente card-list">
						{componentsDirecionador.length > 0
							?
							componentsDirecionador.map(component => 
							<ListGroupItem key={component.id} tag="button" onClick={() => this.componentAction(component.id)}>{component.nome_componente}</ListGroupItem>
							)
							:
							<p>Não existem componentes Direcionadores</p>
						}
					</ListGroup>
				</CardBody>
				</Card>
				<Card>	
				<CardBody>
					<CardTitle className="TipoComponente" tag="h5">Apoio</CardTitle>
					<ListGroup className="Componente card-list">
						{componentsApoio.length > 0 
							?
							componentsApoio.map(component => 
							<ListGroupItem key={component.id} tag="button" onClick={() => this.componentAction(component.id)}>{component.nome_componente}</ListGroupItem>
						)
							:
							<p>Não existem componentes de Apoio</p>
						}
					</ListGroup>
				</CardBody>
				</Card>
			</CardDeck>
			
			<Pesquisa data={this.state.macroprocessos}/>
		  </>		
		)
	}
}

export default Main