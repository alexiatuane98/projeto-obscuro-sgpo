import React,{useState, setState, useEffect} from 'react'
import { useHistory } from "react-router-dom";
import {Table} from 'reactstrap'
import api from "../../services/api"
import Pesquisa from './pesquisa'
import './tables.css'

export default function TableProcessos(props){
		const history = useHistory();
		const [macroprocesso, setMacroprocesso] = useState("");
		const [processo, setProcesso] = useState([]);

		return(
			<>
			<div className="local-tabela">
			<Table className="tabela">
				<thead>
		            <th>      
		            Processo 
		            </th>
		            <th>
		            Nome do Processo 
		            </th>
		            <th>Fronteira de</th>
		            <th>Fronteira até</th>
		            <th>Proprietário</th>
		            <th>Gestor principal</th>
		            <th>PROAD</th>
		            <th>Detalhes</th>
				</thead>
				<tbody>	
					{
						props.processos.length > 0 ?
						props.processos.map((proc) =>
							<tr>
								<td>{proc.codigo}</td>
								<td>{proc.nome_processo}</td>
								<td>{proc.fronteiraDe}</td>
								<td>{proc.fronteirAte}</td>
								<td>{proc.proprietario}</td>
								<td>{proc.gestorPrincipal}</td>
								<td>{proc.proad}</td>
								<td>
								<a href={`processos/${proc.id}`}>
								<button className="btn-sm btn-warning">
									Detalhes
								</button>
								</a>
								</td>
							</tr>
						)
						:
						""
					}
				</tbody>
			</Table>
			{
				props.processos.length === 0 &&
				<p>Selecione um componente e pesquise um macroprocesso</p>
			}
			</div>
			</>
		)
}
