import React from "react";
import { Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import './login.css'
import { Jumbotron } from "react-bootstrap";
export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: false };
  }

  handleValidSubmit = (event, values) => {
    this.setState({ email: values.email });
    console.log(`Login Realixado com sucesso`);
  };

  handleInvalidSubmit = (event, errors, values) => {
    this.setState({ email: values.email, error: true });
    console.log(`Erro ao Logar`);
  };

  render() {
    return (
        <Jumbotron className="login">
            <h3>SGPO - LOGIN</h3>
            <hr className="linhatitulologin"/>
      <AvForm
        onValidSubmit={this.handleValidSubmit}
        onInvalidSubmit={this.handleInvalidSubmit}
      >
        <AvField
        className="campoescrita"
          name="email"
          label="Email"
          type="text"
          validate={{
            required: true,
            email: true
          }}
        />
        <AvField
          name="password"
          label="Password"
          type="password"
          validate={{
            required: {
              value: true,
              errorMessage: "Por favor, insira sua senha"
            },
            pattern: {
              value: "^[A-Za-z0-9]+$",
              errorMessage:
                "Sua senha deve ser composta apenas por letras e números"
            },
            minLength: { 
              value: 6,
              errorMessage: "Sua senha deve ter entre 6 e 16 caracteres"
            },
            maxLength: {
              value: 16,
              errorMessage: "Sua senha deve ter entre 6 e 16 caracteres"
            }
          }}
        />
        <Button id="submit">Acessar</Button>
      </AvForm>
      </Jumbotron>
    );
  }
}