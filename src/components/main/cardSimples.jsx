import React from 'react'
import './cardSimples.css'

export default function CardSimples(props){
	return(
		<div class="card card-dcp">
            <div class="card-header bg-header">{props.header}</div>
            <div class="card-body card-dcp-body">
                {props.children}
            </div>
        </div>
	)
}