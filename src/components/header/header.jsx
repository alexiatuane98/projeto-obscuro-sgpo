import React, { useState } from 'react';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import './header.css'
import Logo from '../../imagens/logo_sgpo.png'

const Header = (props) => {
	const [isOpen, setIsOpen] = useState(false);
  
	const toggle = () => setIsOpen(!isOpen); 
  
	return (
	  <div>
		<Navbar className="menu" expand="md">
		<Navbar.Brand href="/">
		<img 
			src={Logo}
			width="60"
			height="60"
			alt="Logo sgpo"
		/>
		</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse className="nav-menu" id="basic-navbar-nav">
			<Nav className="mr-auto">
			<Nav.Link href="/">inicio</Nav.Link>
			<Nav.Link href="/sobre">sobre</Nav.Link>
			<Nav.Link href="/contato">contato</Nav.Link>
			<NavDropdown title="Cadeia de valor" id="basic-nav-dropdown">
				<NavDropdown.Item href="#action/3.1">Finalístico</NavDropdown.Item>
				<NavDropdown.Item href="#action/3.2">Direcionador</NavDropdown.Item>
				<NavDropdown.Item href="#action/3.3">Apoio</NavDropdown.Item>
				<NavDropdown.Divider />			
			</NavDropdown>
			<Nav.Link href="/login">Login</Nav.Link>
			</Nav>
			</Navbar.Collapse>
		</Navbar>
	  </div>
	);
  }

export default Header;