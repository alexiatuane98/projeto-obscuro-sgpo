import axios from "axios";

const api = axios.create({
  //para api rodando em localhost
  baseURL: "http://127.0.0.1:8000/",
});

export default api;