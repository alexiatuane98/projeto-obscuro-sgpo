import React from 'react'
import Main from './components/main/main'
import Dcp from './components/main/dcp'
import {Switch, Route, Link}  from 'react-router-dom';
import Breadcrumb from './components/header/broadcrumb'
import Pesquisa from './components/main/pesquisa'
import Login from './components/main/login'
import Sobre from './components/main/sobre'
import Contato from './components/main/contato'
import Footer from './components/footer/footer'
export default function Routers(props){
	return(
		<Switch>
			<Route exact path='/'>
				<Main/>
			</Route>
			<Route exact path='/processos/:id'>
				<Dcp/>
			</Route>
			<Route path='/login'>
				<Login/>
			</Route>
			<Route path='/sobre'>
				<Sobre/>
			</Route>
			<Route path='/contato'>
				<Contato/>
			</Route>
		</Switch>
	)
} 