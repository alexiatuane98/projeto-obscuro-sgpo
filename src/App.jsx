import './App.css';
import Header from './components/header/header'
import Rodape from './components/rodape/rodape'
import {BrowserRouter}  from 'react-router-dom';

import Routers from './routers'

function App(props) {
  return (
    <div className="App">
      <Header/>
      <main>
        <BrowserRouter>
        <Routers/>
        </BrowserRouter>
      </main>
      <Rodape/>
    </div>
  );
}

export default App;
